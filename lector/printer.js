const fs = require('fs')
const path = require('path')

const writer = (path, str) => {
    try {
        if (typeof str === 'object') {
            str = JSON.stringify(str, null, 3)
        }
        fs.writeFileSync(path, '', { encoding: 'utf8' })
        fs.appendFileSync(path, str + '\n\n')
    } catch(e) {
        console.error('ERROR')
    } 
}
const getPathResult = pathResult => {
    const base = path.join(pathResult, 'base.txt')
    const fun = path.join(pathResult, 'fun.txt')
    const result = path.join(pathResult, 'result.json')
    return { base, fun, result }
}

const printer = ({pathResult, data, interpretado}) => {
    const { base, result } = getPathResult(pathResult)
    writer(base, data)
    writer(result, interpretado)
}

module.exports = printer