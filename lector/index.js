const { PdfReader } = require('pdfreader')
const interprete = require('./interprete')
const printer = require('./printer')
const fs = require('fs')

const readPDF = async ({ pathPdf, pathResult, content }) => {
    try {
        if(content) {
            var pdfBuffer = Buffer.from(content, 'base64'); 
            const data = await readlines(pdfBuffer)
            const interpretado = await interprete(data)

            return interpretado

        } else {
            //  DEMO con Archivos en el sistema 
            //  Usando de "pathPdf" (ruta del PDF a leer) 
            //  Uso de "pathResult" (ruta del archivo JSON con el contenido interpretado) 

            const pdfBuffer = fs.readFileSync(pathPdf)
            const data = await readlines(pdfBuffer)
            const interpretado = await interprete(data)
            //  Just for debug
            if (pathResult) printer({pathResult, data, interpretado})

            return interpretado
        }
    } catch (error) {
        console.error(error)
        return error
    }
}

const readlines = buffer => new Promise((resolve, reject) => {
    var pdftxt = '';
    const reader = new PdfReader()
    reader.parseBuffer(buffer, function (err, item) {
        if (err) {
            console.error("pdf reader error: " + err);
            reject(err)
        }
        else if (!item) {
            resolve(pdftxt);
        }
        else if (item && item.page) {
            pdftxt += '\n\n\n'
        }
        else if (item.text) {
            pdftxt += item.text + '\n'
        }
    })
})

module.exports = readPDF