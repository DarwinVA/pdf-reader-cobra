const PRL = require('./PRL')
const DEF = require('./DEF')

const readPdf = async data => {
    var data, contenido, error
    try {
        data = data.split('\n').filter(el=>el)
        const actaText = data.shift()
        const acta = actaText.includes('PROVISIONAL') ? 'inicial' : 'final'
        switch(acta) {
            case 'inicial':
                contenido = await PRL(data)
                data = { tipo: 'prl', acta, ...contenido }
                break
            case 'final':
                contenido = await DEF(data)
                data = { tipo: 'def', acta, ...contenido }
                break
            default:
                error = 'No se detectó un tipo correcto (PRL o DEF)'
                break
        }
    } catch(err) {
        console.error(err)
        error = err
    }
    return { data, error }
}

module.exports = readPdf