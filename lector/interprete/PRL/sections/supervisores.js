const getSupervisors = data => {
    data = [...data]
    const inicio = data.findIndex(el=>el==='POR TELEFÓNICA')
    data = data.splice(inicio)
    return {
        supervisorTelefonica: data[3],
        supervisorColaborador: '',
    }
}

module.exports = getSupervisors