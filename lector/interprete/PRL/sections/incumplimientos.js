const INICIO = 'DEFINICIÓN INCUMPLIMIENTOS DE ACCIONES U OMISIONES'
const FIN = 'POR TELEFÓNICA'

const esAgrupacion = text => {
    const tiposAgrupacion = [
        'VOC'
    ]
    return tiposAgrupacion.some(el => text.includes(el))
}
const esDefecto = text => {
    const tiposDefecto = [
        'ESC-'
    ]
    return tiposDefecto.some(el => text.includes(el))
}
const esInfraccion = text => {
    const tiposInfraccion = [
        'LEVE',
        'CORRECTA'
    ]
    return tiposInfraccion.some(el => el === text)
}

const lastItem = arr => arr[arr.length-1]

const getData = arrayData => {
    const result = []
    arrayData.forEach((item, i) => {
        if(esAgrupacion(item)) {
            //  Agrupación
            result.push({})
            lastItem(result).agrupacion = item
        } else if(esDefecto(item)) {
            //  Defecto
            lastItem(result).defecto = item
        } else if(esInfraccion(item)) {
            //  Infracción
            lastItem(result).infraccion = item
        } else {
            const last = lastItem(result)
            if(last.infraccion) {
                //  Comentario
                last.comentario = item
            } else {
                // Descripción
                if(last.descripcion) {
                    last.descripcion += item
                } else {
                    last.descripcion = item
                }
            }
        }
    })
    return result
} 

const getIncumplimientos = async data => {
    try{
        data = [...data]
        const inicioSeccion = data.findIndex(el=>el===INICIO)
        data = data.splice(inicioSeccion)
        const fin = data.findIndex(el=>el===FIN)
        data.length = fin
        const lastField = 'INFRACCIÓN'
        const indice = data.findIndex(el=>el===lastField)
        data = data.splice(indice+1)
        if(data.length <= 4) {
            return []
        }
        const result = await getData(data)
        return result
    } catch(err) {
        console.error(err)
        return err
    }
}

module.exports = getIncumplimientos