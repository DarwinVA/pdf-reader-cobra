const { getFields, groupArray } = require('./../../utils')

const trabajador = [    
    'DATOS TRABAJADOR',
    'NIF',
    'NOMBRE',
    'SUBCONTRATA',
    'INCUMPLIMIENTOS'
]
const formModelo = data => {
    return new Promise((resolve,reject) => {
        try {
            data = data.concat()
            data = groupArray(item=>item.includes('DATOS TRABAJADOR'), data)
            const result = []
            const pushTrabajador = async arr => {
                try {
                    arr = [...arr]
                    const numero = [...arr.shift()].pop()
                    const content = await getFields(trabajador, arr, (field,content) => content.includes(field))
                    const trabajadorData = {numero, ...content}
                    result.push( trabajadorData )
                } catch(e) {
                    console.error('Error pusheando data de 1 trabajador')
                }
                return null
            }
            Promise.all(data.map(pushTrabajador))
                .then(() => {
                    resolve(result)
                })
        } catch(e) {
            reject(e)
        }
    })
}

const sectionTrabajadores = async data => {
    data = [...data]
    const inicio = data.findIndex(el=>el.includes('DATOS TRABAJADOR'))
    data = data.splice(inicio)
    const result = await formModelo(data)
    return result
}

module.exports = sectionTrabajadores