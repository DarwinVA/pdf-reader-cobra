const { getFields } = require('./../../utils')
const headers = [
    'PROVINCIA', 'CONTRATO', 'TIPO AUDITORIA',
    'EMPRESA', 'LOCALIDAD', 'Nº AUDITORIA',
    'DOCUMENTO', { text: 'TITULO', size: 1 }, 'F AUDITORIA',
    'LOCALIZACIÓN', 'ENCARGADO', 'INTERLOCUTOR PRL', 
    { text: 'OBSERVACIONES', size: 9 }, 'DATOS OBRA'
]

const getCabecera = async data => {
    data = [...data]
    const fin = data.findIndex(el=>el==='DEFINICIÓN INCUMPLIMIENTOS DE ACCIONES U OMISIONES')
    data = data.splice(0, fin);
    const result = await getFields(headers, data)
    return result
}

module.exports = getCabecera