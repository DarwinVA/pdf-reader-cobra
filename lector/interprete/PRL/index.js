const getCabecera = require('./sections/cabecera')
const getSupervisores = require('./sections/supervisores')
const getIncumplimientos = require('./sections/incumplimientos')
const getTrabajadores = require('./sections/trabajadores')

const interpretePrl = async data => {
    try {
        const result = {
            campos: await getCabecera(data),
            incumplimientos: await getIncumplimientos(data),
            ...await getSupervisores(data),
            trabajadores: await getTrabajadores(data),
        }
        return result
    } catch(err) {
        console.error(err)
        return err
    }
}

module.exports = interpretePrl