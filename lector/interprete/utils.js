const utils = {
    getFields: (arrayFields, arrayContent, exact) => {
        arrayFields = [...arrayFields]
        const pendingFields = []
        const object = {}
        return new Promise((resolve, reject) => {
            try {
                arrayContent.forEach(content => {
                    if (arrayFields.length || pendingFields.length) {
                        const indexField = arrayFields.findIndex(it => {
                            let field = it.text || it
                            var result
                            if(it.eval) {
                                result = it.eval(field, content)
                            } else {
                                switch (typeof exact) {
                                    case 'number':
                                        switch(exact) {
                                            case -1:
                                                result = field.includes(content)
                                                break
                                            case 0:
                                                result = field === content
                                                break
                                            case 1:
                                                result = content.includes(field)
                                                break
                                            default:
                                                break
                                        }
                                        break
                                    case 'function':
                                        result = exact(field, content)
                                        break
                                    case 'undefined':
                                        result = field === content
                                    default:
                                        break
                                }
                            }
                            return result
                        })
                        if (indexField >= 0) {
                            const field = arrayFields.splice(indexField, 1)
                            pendingFields.push(field[0])
                        } else if (pendingFields.length) {
                            var field = pendingFields[0]
                            if (field.size) {
                                if (field.size === 1) {
                                    field = pendingFields.shift()
                                    object[field.text] = content
                                } else {
                                    if (field.size === field.done) {
                                        field = pendingFields.shift()
                                        object[field.text] += content
                                    } else if (!field.done) {
                                        object[field.text] = content + ' '
                                        field.done = 2
                                    } else {
                                        object[field.text] += content + ' '
                                        field.done++
                                    }
                                }
                            } else {
                                field = pendingFields.shift()
                                object[field] = content
                            }
                        }
                    } else {
                        resolve(object)
                    }
                })
                resolve(object)
            } catch (e) {
                console.error(e)
                reject(e)
            }
        })
    },
    groupArray: (separador, array) => {
        return array.reduce((acum, item) => {
            if(separador(item)) {
                acum.push([])
            }
            acum[acum.length-1].push(item)
            return acum
        }, [])
    }
}

module.exports = utils