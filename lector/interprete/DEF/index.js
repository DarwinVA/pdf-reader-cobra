const seccionCabecera = require('./sections/cabecera')
const seccionIncumplimientos = require('./sections/incumplimientos')
const seccionSupervisores = require('./sections/supervisores')

const interpretePrl = async data => {
    const result = {
        cabecera: await seccionCabecera(data),
        incumplimientos: seccionIncumplimientos(data),
        ...await seccionSupervisores(data),
    }
    return result
}

module.exports = interpretePrl