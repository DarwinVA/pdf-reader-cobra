const { getFields } = require('./../../utils')
const headers = [
    'PROVINCIA',
    'CONTRATO',
    'TIPO AUDITORIA',
    'EMPRESA',
    'LOCALIDAD',
    'Nº AUDITORIA',
    'DOCUMENTO', 
    'LOCALIZACIÓN',
    'F AUDITORIA',
    {text: 'TITULO', size: 2}, 
    'ENCARGADO', 
    'INTERLOCUTOR PRL', 
    'OBSERVACIONES',
    'DATOS OBRA',
    'DEFINICIÓN INCUMPLIMIENTOS DE ACCIONES U OMISIONES',
    'CALIFICACIÓN'
]

const sectionHeaders = async data => {
    try {
        return await getFields(headers, data)
    } catch(err) {
        console.error(err)
        return err
    }
}

module.exports = sectionHeaders