const INICIO = 'RELACIÓN DE INCUMPLIMIENTOS'
const FIN = 'POR TELEFÓNICA'

const getIncumplimientos = data => {
    data = [...data]
    const inicioSeccion = data.findIndex(el=>el===INICIO)
    data = data.splice(inicioSeccion)
    const fin = data.findIndex(el=>el===FIN)
    data.length = fin
    const TotalIndex = data.findIndex(el => el === 'TOTAL DEPON')
    data = data.splice(TotalIndex)
    const totalDepon = data[1]
    return { totalDepon }
}

module.exports = getIncumplimientos