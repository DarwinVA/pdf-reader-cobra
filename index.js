const lectorPDF = require('./lector')

const DEF_KO = {
    pathPdf: './workSpace/pdfs/DEF_KO.PDF',
    pathResult: './workSpace/results/DEF_KO',
}
const DEF_OK = {
    pathPdf: './workSpace/pdfs/DEF_OK.PDF',
    pathResult: './workSpace/results/DEF_OK',
}

const PRL_KO = {
    pathPdf: './workSpace/pdfs/PRL_KO.PDF',
    pathResult: './workSpace/results/PRL_KO',
}
const PRL_OK = {
    pathPdf: './workSpace/pdfs/PRL_OK.PDF',
    pathResult: './workSpace/results/PRL_OK',
};

(async ()=>{
    const data = await lectorPDF(DEF_KO)
    console.log('END');
})();